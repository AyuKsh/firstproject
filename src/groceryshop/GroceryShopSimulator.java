

package groceryshop;

import java.util.Scanner;

public class GroceryShopSimulator {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        CartOperations c1=new CartOperations();
        boolean repeat=true;
        while (repeat){
            System.out.println("Welcome to Grocery Shop");
            System.out.println("1:View Grocery Items");
            System.out.println("2:Add Item to Cart");
            System.out.println("3:Remove Item from Cart");
            System.out.println("4:View All Cart Items");
            System.out.println("5:Checkout");
            System.out.println("6:Pay Bill");
            System.out.println("7:Exit");
            int choice= sc1.nextInt();
            switch (choice){
                case 1:
                    //System.out.println("View Grocery Items");
                    c1.viewAllItems();
                    break;
                case 2:
                    //System.out.println("Add Item to Cart");
                    System.out.println("Enter Item Name");
                    String name= sc1.next();
                    System.out.println("Enter Qty");
                    int qty= sc1.nextInt();
                    c1.addItemToCart(name,qty);
                    break;
                case 3:
                    //System.out.println("Remove Item From Cart");
                    System.out.println("Enter Product Name");
                    String item= sc1.next();
                    c1.removeCartItems(item);
                    break;
                case 4:
                    //System.out.println("View cart Items");
                    c1.viewCartItems();
                    break;
                case 5:
                    //System.out.println("Checkout");
                    c1.checkout();
                    break;
                case 6:
                    //System.out.println("Pay Bill");
                    System.out.println("Enter Amount To Be Paid");
                    double amt= sc1.nextDouble();
                    c1.billPayment(amt);
                    break;
                case 7:
                    if (c1.totalAmount==0.0){
                        repeat=false;
                    }
                    else {
                        System.out.println("First Make Bill Payment");
                    }
                    break;
                default:
                    System.out.println("Invalid choice");
                    break;
            }
        }
    }
}

