package groceryshop;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;



    //Business logic class
    public class CartOperations {
        HashMap<String,Double> groceryItems=new HashMap<>();
        LinkedHashMap<String,Double> cartItems=new LinkedHashMap<>();
        double totalAmount=0.0;

        {
            groceryItems.put("SUGAR",45.0);
            groceryItems.put("TEA",70.0);
            groceryItems.put("MILK",30.0);
            groceryItems.put("TOMATO",25.0);
            groceryItems.put("POTATO",40.0);
        }
        void viewAllItems(){
            //display all grocery items
            Set<Map.Entry<String,Double>> items=groceryItems.entrySet();
            System.out.println("ITEM-NAME\t\tPRICE");
            System.out.println("============================");
            for (Map.Entry<String,Double> entry:items){
                System.out.println(entry.getKey()+"\t\t\t"+entry.getValue());
            }
            System.out.println("============================");
        }
        void addItemToCart(String name,int qty){
            //add items to shopping cart
            boolean status=groceryItems.containsKey(name);
            if (status){
                //double price=groceryItems.get(name);
                cartItems.put(name,groceryItems.get(name)*qty);
                System.out.println("Item Added Successfully");
            }else {
                System.out.println("Invalid Product Name");
            }
            System.out.println("============================");
        }
        void removeCartItems(String name){
            //remove cart items
            boolean status=cartItems.containsKey(name);
            if (status){
                cartItems.remove(name);
                System.out.println("Product Removed From Cart");
            }else {
                System.out.println("First Add to Cart");
            }
            System.out.println("============================");
        }
        void viewCartItems(){
            //display cart items
            Set<Map.Entry<String,Double>> items=cartItems.entrySet();
            System.out.println("ITEM-NAME\t\tTOTAL-PRICE");
            System.out.println("============================");
            for (Map.Entry<String,Double> entry:items){
                System.out.println(entry.getKey()+"\t\t\t"+entry.getValue());
            }
            System.out.println("============================");
        }
        void checkout(){
            //display total bill amount with description
            System.out.println("ITEM-NAME\t\t\tU.PRICE\t\t\tTOTAL");
            System.out.println("============================");
            Set<Map.Entry<String,Double>> items=cartItems.entrySet();
            for (Map.Entry<String,Double> e:items){
                System.out.println(e.getKey()+"\t\t\t"+groceryItems.get(e.getKey())+"\t\t\t"+e.getValue());
                totalAmount+=e.getValue();
            }
            System.out.println("============================");
            System.out.println("TOTAL PAYABLE AMOUNT :"+totalAmount);
        }
        void billPayment(double amount){
            //settle bill payment
            if (amount==totalAmount){
                totalAmount-=amount;
                System.out.println("Bill Payment Successful");
                cartItems.clear();
            }else {
                System.out.println("Enter Correct Amount");
            }
        }
    }


